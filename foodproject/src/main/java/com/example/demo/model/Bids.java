package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
@Table(name="bids")
public class Bids {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	
	public Integer BID_ID;
	
	public Integer USER_ID;
	
	public Integer FOOD_ID;
	
	private Integer BID_PRICE;
	
}
